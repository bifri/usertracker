package io.bifri.usertracker.util

import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.Function


fun <T : Observable<Opt<E?>>, E> T.pickValue(): Observable<E> =
        filter { it.isPresent }.map { it.value }

fun <T : Single<Opt<E?>>, E> T.pickValue(): Maybe<E> =
        filter { it.isPresent }.map { it.value }

fun <T : Maybe<Opt<E?>>, E> T.pickValue(): Maybe<E> =
        filter { it.isPresent }.map { it.value }

fun <T : Any> Observable<T>.mapIndexed(): Observable<Pair<Long, T>> =
        map(object : Function<T, Pair<Long, T>> {
            var counter: Long = 0L

            override fun apply(t: T): Pair<Long, T> = counter++ to t
        })