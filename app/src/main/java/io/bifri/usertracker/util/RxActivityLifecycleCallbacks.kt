package io.bifri.usertracker.util

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.lifecycle.Lifecycle.Event
import androidx.lifecycle.Lifecycle.Event.*
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.bifri.usertracker.app.App
import io.reactivex.Observable


class RxActivityLifecycleCallbacks(app: App) : Application.ActivityLifecycleCallbacks {

    private val activityEventsInternal: Relay<Pair<Activity, Event>> =
            PublishRelay.create()

    val activityEvents: Observable<Pair<Activity, Event>> = activityEventsInternal.hide()

    init {
        app.registerActivityLifecycleCallbacks(this)
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        activityEventsInternal.accept(activity to ON_CREATE)
    }

    override fun onActivityStarted(activity: Activity) {
        activityEventsInternal.accept(activity to ON_START)
    }

    override fun onActivityResumed(activity: Activity) {
        activityEventsInternal.accept(activity to ON_RESUME)
    }

    override fun onActivityPaused(activity: Activity) {
        activityEventsInternal.accept(activity to ON_PAUSE)
    }

    override fun onActivityStopped(activity: Activity) {
        activityEventsInternal.accept(activity to ON_STOP)
    }

    override fun onActivityDestroyed(activity: Activity) {
        activityEventsInternal.accept(activity to ON_DESTROY)
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
        // Not tracked
    }

}