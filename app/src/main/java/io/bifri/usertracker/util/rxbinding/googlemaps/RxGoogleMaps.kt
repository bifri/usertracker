package io.bifri.usertracker.util.rxbinding.googlemaps

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.sdoward.rxgooglemap.MapObservableProvider
import io.reactivex.Maybe

fun mapAndViewReady(
        mapFragment: SupportMapFragment,
        mapObservableProvider: MapObservableProvider
): Maybe<GoogleMap> = mapObservableProvider.getMapReadyObservable().firstElement()
        .compose(MapAndViewReadyTransformer(mapFragment))