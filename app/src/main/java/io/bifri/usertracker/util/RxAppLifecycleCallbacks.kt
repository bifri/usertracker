package io.bifri.usertracker.util

import androidx.lifecycle.Lifecycle.Event
import androidx.lifecycle.Lifecycle.Event.*
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable


class RxAppLifecycleCallbacks(
        processLifecycleOwner: LifecycleOwner
) : LifecycleObserver {

    private val appEventsInternal: Relay<Event> = PublishRelay.create()

    val appEvents: Observable<Event> = appEventsInternal

    init {
        processLifecycleOwner.lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(ON_CREATE) fun onAppCreated() {
        appEventsInternal.accept(ON_CREATE)
    }

    @OnLifecycleEvent(ON_START) fun onAppStarted() {
        appEventsInternal.accept(ON_START)
    }

    @OnLifecycleEvent(ON_RESUME) fun onAppResumed() {
        appEventsInternal.accept(ON_RESUME)
    }

    @OnLifecycleEvent(ON_PAUSE) fun onAppPaused() {
        appEventsInternal.accept(ON_PAUSE)
    }

    @OnLifecycleEvent(ON_STOP) fun onAppStopped() {
        appEventsInternal.accept(ON_STOP)
    }

    @OnLifecycleEvent(ON_DESTROY) fun onAppDestroyed() {
        appEventsInternal.accept(ON_DESTROY)
    }

}