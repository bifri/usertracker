package io.bifri.usertracker.util

val SEMICOLON_REGEX = ";".toRegex()
val COMMA_REGEX = ",".toRegex()