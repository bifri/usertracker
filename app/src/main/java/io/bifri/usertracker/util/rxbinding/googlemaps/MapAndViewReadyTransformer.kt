package io.bifri.usertracker.util.rxbinding.googlemaps

import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle.Event.ON_RESUME
import androidx.lifecycle.Lifecycle.Event.ON_START
import com.google.android.gms.maps.GoogleMap
import com.jakewharton.rxbinding3.view.layoutChanges
import com.trello.lifecycle2.android.lifecycle.AndroidLifecycle
import io.reactivex.Maybe
import io.reactivex.MaybeSource
import io.reactivex.MaybeTransformer

class MapAndViewReadyTransformer(
        private val mapFragment: Fragment
) : MaybeTransformer<GoogleMap, GoogleMap> {

    override fun apply(mapReady: Maybe<GoogleMap>): MaybeSource<GoogleMap> {
        val fragmentReady = AndroidLifecycle.createLifecycleProvider(mapFragment).lifecycle()
                .filter {
                    when (it) {
                        ON_START, ON_RESUME -> true
                        else -> false
                    }
                }
                .map { mapFragment }
                .firstElement()
        val viewReady = fragmentReady
                .flatMap {
                    viewCompletedLayoutSuccessfullyOrLayoutChanges(it.requireView())
                }
        return Maybe.zip(mapReady, viewReady) { googleMap, _ -> googleMap }
    }

    private fun viewCompletedLayoutSuccessfullyOrLayoutChanges(
            mapView: View
    ): Maybe<Unit> = viewCompletedLayoutSuccessfully(mapView)
            // Map has not undergone layout, register a View layout changes observer
            .concatWith(mapView.layoutChanges().firstElement())
            .firstElement()

    private fun viewCompletedLayoutSuccessfully(
            mapView: View
    ): Maybe<Unit> = Maybe.fromCallable { mapView.width != 0 && mapView.height != 0 }
            .filter { it }
            .map {  }

}