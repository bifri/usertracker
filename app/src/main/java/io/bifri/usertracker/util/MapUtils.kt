package io.bifri.usertracker.util

import android.animation.ValueAnimator
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.view.animation.BounceInterpolator
import android.view.animation.Interpolator
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import kotlin.math.max

fun animateMarkerClick(marker: Marker) {
    // This causes the marker to bounce into position when it is clicked
    val handler = Handler(Looper.getMainLooper())
    val start = SystemClock.uptimeMillis()
    val duration = 1_000L
    val interpolator: Interpolator = BounceInterpolator()
    handler.post(object : Runnable {
        override fun run() {
            val elapsed = SystemClock.uptimeMillis() - start
            val t = max(1 - interpolator.getInterpolation(elapsed.toFloat() / duration), 0f)
            marker.setAnchor(0.5f, 1.0f + 2 * t)
            if (t > 0.0) {
                // Post again 16ms later.
                handler.postDelayed(this, 16)
            }
        }
    })
}

fun Marker.changePositionSmoothly(newLatitude: Double, newLongitude: Double) {
    val animation = ValueAnimator.ofFloat(0f, 100f)
    var previousStep = 0f
    val deltaLatitude = newLatitude - position.latitude
    val deltaLongitude = newLongitude - position.longitude
    animation.duration = 1_000L
    animation.addUpdateListener { updatedAnimation ->
        val deltaStep = updatedAnimation.animatedValue as Float - previousStep
        previousStep = updatedAnimation.animatedValue as Float
        position = LatLng(
                position.latitude + deltaLatitude * deltaStep * 1 / 100,
                position.longitude + deltaStep * deltaLongitude * 1 / 100
        )
    }
    animation.start()
}