package io.bifri.usertracker.util

import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.core.content.getSystemService


fun TextView.setTextResIdIfChanged(@StringRes newTextResId: Int?) =
        setTextIfChanged(newTextResId?.let { resources.getString(it) })

fun TextView.setTextIfChanged(newText: CharSequence?) {
    if (isTextChanged(newText)) text = newText
}

fun TextView.isTextChanged(newText: CharSequence?) = text.isTextChanged(newText)

fun CharSequence?.isTextChanged(newText: CharSequence?) =
        this?.toString().orEmpty() != newText?.toString().orEmpty()

fun View.hideKeyboard() {
    val imm: InputMethodManager = context.getSystemService() ?: return
    imm.hideSoftInputFromWindow(windowToken, 0)
}