package io.bifri.usertracker.network.response

data class UsersResponse(val users: List<User>) : Response

data class User(
        val id: String,
        val name: String,
        val imageUrl: String,
        val latitude: Double,
        val longitude: Double
)