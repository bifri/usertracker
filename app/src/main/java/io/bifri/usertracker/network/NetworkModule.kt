package io.bifri.usertracker.network

import dagger.Module
import dagger.Provides
import io.bifri.usertracker.di.PerApp
import io.bifri.usertracker.exception.ExceptionService
import io.bifri.usertracker.network.service.ResponseMapper
import io.bifri.usertracker.network.service.TcpClient
import io.bifri.usertracker.network.service.UserLocationService


@Module class NetworkModule {

    @Provides @PerApp fun provideTcpClient(
            exceptionService: ExceptionService
    ): TcpClient = TcpClient(exceptionService)

    @Provides @PerApp fun provideResponseMapper(): ResponseMapper = ResponseMapper()

    @Provides @PerApp fun provideUserLocationService(
            tcpClient: TcpClient,
            responseMapper: ResponseMapper
    ): UserLocationService = UserLocationService(tcpClient, responseMapper)

}