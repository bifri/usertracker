package io.bifri.usertracker.network.response

data class LocationResponse(val location: Location) : Response

data class Location(
        val userId: String,
        val latitude: Double,
        val longitude: Double
)