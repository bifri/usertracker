package io.bifri.usertracker.network.service

import io.bifri.usertracker.network.response.*
import io.bifri.usertracker.util.COMMA_REGEX
import io.bifri.usertracker.util.SEMICOLON_REGEX


private const val USER_LIST_RESPONSE_NAME = "USERLIST"
private const val UPDATE_RESPONSE_NAME = "UPDATE"

class ResponseMapper {

    fun toResponse(str: String): Response = when {
        str.startsWith(USER_LIST_RESPONSE_NAME) -> toUsersResponse(str)
        str.startsWith(UPDATE_RESPONSE_NAME) -> toLocationResponse(str)
        else -> throw IllegalArgumentException("Unknown server response: $str")
    }

    private fun toUsersResponse(str: String): UsersResponse {
        val usersStr = str.substringAfter(USER_LIST_RESPONSE_NAME).trimStart()
        val rawUsers = usersStr.split(SEMICOLON_REGEX)
        val users = rawUsers.asSequence()
                .map { it.split(COMMA_REGEX) }
                .filter {
                    it.size == 5
                            && it[3].toDoubleOrNull() != null
                            && it[4].toDoubleOrNull() != null
                }
                .map { User(it[0], it[1], it[2], it[3].toDouble(), it[4].toDouble()) }
                .toList()
        return UsersResponse(users)
    }

    private fun toLocationResponse(str: String): LocationResponse {
        val locationStr = str.substringAfter(UPDATE_RESPONSE_NAME).trimStart()
        val rawLocation = locationStr.split(COMMA_REGEX)
        if (rawLocation.size != 3
                || rawLocation[1].toDoubleOrNull() == null
                || rawLocation[2].toDoubleOrNull() == null
        ) {
            throw IllegalArgumentException("Malformed server $UPDATE_RESPONSE_NAME response")
        }
        val location = Location(
                rawLocation[0],
                rawLocation[1].toDouble(),
                rawLocation[2].toDouble()
        )
        return LocationResponse(location)
    }

}

