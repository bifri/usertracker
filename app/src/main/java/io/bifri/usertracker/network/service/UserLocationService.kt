package io.bifri.usertracker.network.service

import io.bifri.usertracker.network.response.LocationResponse
import io.bifri.usertracker.network.response.Response
import io.bifri.usertracker.network.response.UsersResponse
import io.reactivex.Observable

private const val AUTHORIZE_EMAIL = "ai.latvia@gmail.com"

class UserLocationService(
        private val tcpClient: TcpClient,
        private val responseMapper: ResponseMapper
) {
    private val responses: Observable<Response> = tcpClient.messages()
            .map { responseMapper.toResponse(it) }

    val initialized get() = tcpClient::initialized

    fun authorize() = tcpClient.sendMessage(AuthorizeCommand(AUTHORIZE_EMAIL).commandStr)

    fun users(): Observable<UsersResponse> = responses
            .filter { it is UsersResponse }
            .cast(UsersResponse::class.java)

    fun newLocation(): Observable<LocationResponse> = responses
            .filter { it is LocationResponse }
            .cast(LocationResponse::class.java)

}