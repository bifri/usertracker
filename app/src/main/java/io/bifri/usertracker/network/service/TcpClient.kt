package io.bifri.usertracker.network.service

import androidx.annotation.WorkerThread
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.bifri.usertracker.exception.ExceptionService
import io.bifri.usertracker.util.logd
import io.reactivex.Observable
import java.io.BufferedReader
import java.io.IOException
import java.io.PrintWriter
import java.net.InetAddress
import java.net.Socket
import java.util.concurrent.Executors


private const val HOST = "ios-test.printful.lv"
private const val PORT = 6111

class TcpClient(private val exceptionService: ExceptionService) {
    @Volatile private var socket: Socket? = null
    @Volatile private var reader: BufferedReader? = null
    @Volatile private var writer: PrintWriter? = null
    @Volatile private var isSubscribed: Boolean = false
    private val readerExecutor = Executors.newSingleThreadExecutor()
    private val initialized: Relay<Boolean> = BehaviorRelay.createDefault(false)
            .toSerialized()
    private val messages: Relay<String> = PublishRelay.create()

    @WorkerThread @Synchronized fun subscribe() {
        if (isSubscribed) return
        val socket: Socket
        val reader: BufferedReader
        val writer: PrintWriter
        try {
            val inetAddress = InetAddress.getByName(HOST)
            socket = Socket(inetAddress, PORT)
            reader = socket.getInputStream().bufferedReader()
            writer = PrintWriter(socket.getOutputStream().bufferedWriter(), true)
        } catch (e: IOException) {
            unsubscribe()
            exceptionService.processException(e)
            return
        }
        this.socket = socket
        this.reader = reader
        this.writer = writer
        isSubscribed = true
        initialized.accept(true)
        readerExecutor.execute {
            while (isSubscribed) {
                val message: String?
                try {
                    message = reader.readLine()
                } catch (e: IOException) {
                    exceptionService.processException(e)
                    break
                }
                if (message != null) {
                    logd(message)
                    messages.accept(message)
                } else break
            }
        }
    }

    @Synchronized fun unsubscribe() {
        try {
            socket?.close()
        } catch (ignored: IOException) {}
        this.socket = null
        this.reader = null
        this.writer = null
        isSubscribed = false
        initialized.accept(false)
    }

    fun initialized(): Observable<Boolean> = initialized

    fun messages(): Observable<String> = messages

    @WorkerThread fun sendMessage(message: String) {
        try {
            writer?.println(message)
        } catch (e: IOException) {
            exceptionService.processException(e)
        }
    }

}