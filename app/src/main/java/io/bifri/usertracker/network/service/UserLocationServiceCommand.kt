package io.bifri.usertracker.network.service

sealed class UserLocationServiceCommand {
    abstract val commandStr: String
}


private const val AUTHORIZE_COMMAND_NAME = "AUTHORIZE"

data class AuthorizeCommand(private val email: String) : UserLocationServiceCommand() {

    override val commandStr: String get() = "$AUTHORIZE_COMMAND_NAME $email"
}