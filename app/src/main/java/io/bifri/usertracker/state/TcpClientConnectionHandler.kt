package io.bifri.usertracker.state

import androidx.lifecycle.Lifecycle.Event.ON_START
import androidx.lifecycle.Lifecycle.Event.ON_STOP
import io.bifri.usertracker.exception.ExceptionService
import io.bifri.usertracker.network.service.TcpClient
import io.bifri.usertracker.util.RxActivityLifecycleCallbacks
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers.io

class TcpClientConnectionHandler(
        private val rxActivityLifecycleCallbacks: RxActivityLifecycleCallbacks,
        private val exceptionService: ExceptionService,
        private val tcpClient: TcpClient
) {

    fun register(): Disposable = rxActivityLifecycleCallbacks.activityEvents
            .observeOn(io())
            .doOnNext {
                when (it.second) {
                    ON_START -> tcpClient.subscribe()
                    ON_STOP -> tcpClient.unsubscribe()
                    else -> {}
                }
            }
            .subscribeBy(onError = exceptionService::processException)

}