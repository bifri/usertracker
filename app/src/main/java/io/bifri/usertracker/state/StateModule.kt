package io.bifri.usertracker.state

import dagger.Module
import dagger.Provides
import io.bifri.usertracker.di.PerApp
import io.bifri.usertracker.exception.ExceptionService
import io.bifri.usertracker.network.service.TcpClient
import io.bifri.usertracker.util.RxActivityLifecycleCallbacks


@Module class StateModule {

    @Provides @PerApp fun provideTcpClientConnectionHandler(
            rxActivityLifecycleCallbacks: RxActivityLifecycleCallbacks,
            exceptionService: ExceptionService,
            tcpClient: TcpClient
    ): TcpClientConnectionHandler = TcpClientConnectionHandler(
            rxActivityLifecycleCallbacks, exceptionService, tcpClient
    )

}