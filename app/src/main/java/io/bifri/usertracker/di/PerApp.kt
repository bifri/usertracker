package io.bifri.usertracker.di


import javax.inject.Scope

/**
 * Custom scope for global application singletons
 */
@Scope annotation class PerApp
