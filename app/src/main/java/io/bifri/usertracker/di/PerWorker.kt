package io.bifri.usertracker.di


import javax.inject.Scope

/**
 * Custom scope for Android Workers singletons
 */
@Scope annotation class PerWorker