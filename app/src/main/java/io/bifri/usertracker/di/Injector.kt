package io.bifri.usertracker.di

interface Injector {
    fun inject(target: Any)
}