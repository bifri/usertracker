package io.bifri.usertracker.di

const val APP_CONTEXT = "appContext"
const val ACTIVITY_CONTEXT = "activityContext"
const val APP_INJECTOR = "appInjector"
const val PROCESS_LIFECYCLE_OWNER = "processLifecycleOwner"