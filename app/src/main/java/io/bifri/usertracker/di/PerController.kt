package io.bifri.usertracker.di


import javax.inject.Scope

/**
 * Custom scope for controller (activity, fragment) singletons
 */
@Scope annotation class PerController