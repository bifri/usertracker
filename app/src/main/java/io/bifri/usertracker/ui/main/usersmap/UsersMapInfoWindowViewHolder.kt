package io.bifri.usertracker.ui.main.usersmap

import android.graphics.Bitmap
import android.location.Address
import com.google.android.gms.maps.model.Marker
import io.bifri.usertracker.databinding.ItemUsersMapInfoWindowBinding
import io.bifri.usertracker.util.setTextIfChanged

class UsersMapInfoWindowViewHolder(
        private val itemBinding: ItemUsersMapInfoWindowBinding
) {

    fun bind(marker: Marker, image: Bitmap?, address: Address?) {
        with(itemBinding) {
            imageViewUsersMapInfoWindowAvatar.setImageBitmap(image)
            imageViewUsersMapInfoWindowTitle.setTextIfChanged(marker.title)

            val addressStr = if (address != null && address.maxAddressLineIndex >= 0) {
                address.getAddressLine(0)
            } else ""
            imageViewUsersMapInfoWindowAddress.setTextIfChanged(addressStr)
        }
    }

}