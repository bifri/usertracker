package io.bifri.usertracker.ui.main.usersmap

import android.app.Application
import androidx.lifecycle.Lifecycle.Event.ON_START
import com.google.android.gms.maps.model.LatLng
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.bifri.usertracker.base.viewmodel.BaseViewModel
import io.bifri.usertracker.exception.ExceptionService
import io.bifri.usertracker.network.service.UserLocationService
import io.bifri.usertracker.util.Event
import io.bifri.usertracker.util.opt
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers.io
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider


private const val DEFAULT_MAX_ADDRESSES = 1

class UsersMapFragmentVM(
        application: Application,
        private val exceptionService: ExceptionService,
        private val userLocationService: UserLocationService,
        private val locationProvider: ReactiveLocationProvider
) : BaseViewModel(application) {

    private val onMarkerClickIntent: Relay<Pair<String, LatLng>> = PublishRelay.create()
    private val usersMapState: Relay<UsersMapState> = BehaviorRelay.create()

    init {
        subscribeToDataStore()
    }

    fun onMarkerClick() = onMarkerClickIntent::accept

    fun usersMapState(): Observable<UsersMapState> = usersMapState

    override fun subscribeToDataStoreInternal(compositeDisposable: CompositeDisposable) {
        val authorize = lifecycleProvider.lifecycle()
                .filter { it == ON_START }
                .switchMap { _ ->
                    userLocationService.initialized()
                            .filter { it }
                            .firstElement()
                            .observeOn(io())
                            .doOnSuccess { userLocationService.authorize() }
                            .ignoreElement()
                            .toObservable<PartialState>()
                }

        val users = userLocationService.users().map { LoadedUsers(it.users) }

        val location = userLocationService.newLocation().map { LoadedLocation(it.location) }

        val address = onMarkerClickIntent
                .observeOn(io())
                .switchMap { (userId, initialLatLng) ->
                    userLocationService.newLocation()
                            .filter { it.location.userId == userId }
                            .map { LatLng(it.location.latitude, it.location.longitude) }
                            .startWith(initialLatLng)
                            .switchMapMaybe { latLng ->
                                locationProvider.getReverseGeocodeObservable(
                                        latLng.latitude,
                                        latLng.longitude,
                                        DEFAULT_MAX_ADDRESSES
                                )
                                        .firstElement()
                                        .map { addresses ->
                                            val address = addresses.getOrNull(0)
                                            LoadedAddress(address)
                                        }
                            }
                }

        val allIntentsObservable = Observable.mergeArray(
                authorize, users, location, address
        )
        val initialState = UsersMapState()

        compositeDisposable.addAll(
                allIntentsObservable
                        .scan(initialState, ::viewStateReducer)
                        .distinctUntilChanged()
                        .observeOn(mainThread())
                        .subscribeBy(
                                onNext = usersMapState::accept,
                                onError = exceptionService::processException
                        )
        )
    }

    private fun viewStateReducer(
            previousState: UsersMapState,
            changes: PartialState
    ): UsersMapState = when (changes) {
        is LoadedUsers -> previousState.copy(users = Event(changes.users))
        is LoadedLocation -> previousState.copy(location = Event(changes.location))
        is LoadedAddress -> previousState.copy(address = Event(changes.address.opt))
    }

}