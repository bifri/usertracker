package io.bifri.usertracker.ui.main.usersmap

import android.graphics.Bitmap
import android.location.Address
import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import com.bumptech.glide.Glide
import com.bumptech.glide.request.FutureTarget
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import com.sdoward.rxgooglemap.MapObservableProvider
import io.bifri.usertracker.R
import io.bifri.usertracker.base.fragment.NoArgsBaseFragment
import io.bifri.usertracker.exception.ExceptionService
import io.bifri.usertracker.network.response.Location
import io.bifri.usertracker.network.response.User
import io.bifri.usertracker.util.Opt
import io.bifri.usertracker.util.animateMarkerClick
import io.bifri.usertracker.util.changePositionSmoothly
import io.bifri.usertracker.util.mapIndexed
import io.bifri.usertracker.util.rxbinding.googlemaps.mapAndViewReady
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.Observables
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers.io
import javax.inject.Inject


private const val IMAGE_WIDTH_IN_PIXELS = 216
private  const val IMAGE_HEIGHT_IN_PIXELS = 216

class UsersMapFragment : NoArgsBaseFragment<UsersMapState>() {

    @Inject override lateinit var viewBinder: UsersMapFragmentVB
    @Inject lateinit var exceptionService: ExceptionService
    private val markerClickIntent: Relay<Pair<String, LatLng>> = PublishRelay.create()
    private var mapObservableProvider: MapObservableProvider? = null
    private var viewCompositeDisposable: CompositeDisposable? = null
    private var usersRelay: Relay<List<User>> = BehaviorRelay.create()
    private var locationRelay: Relay<Location> = PublishRelay.create()
    private var addressRelay: Relay<Opt<Address?>> = PublishRelay.create()
    private var map: GoogleMap? = null
    private var markersAdapter: UsersMapMarkersAdapter? = null
    private var infoWindowAdapter: UsersMapInfoWindowAdapter? = null
    private var markerToUser: Map<Marker, User>? = null
    override val titleResId @StringRes get() = R.string.usersMapTitle
    override val layoutId @LayoutRes get() = R.layout.fragment_users_map

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        subscribeMap()
    }

    override fun onDestroyView() {
        mapObservableProvider = null
        unsubscribeMap()
        super.onDestroyView()
    }

    override fun subscribeInternal(cd: CompositeDisposable) {
        val markerClick = mapObservableProvider!!.getMarkerClickObservable()
                .map { it to markerToUser?.get(it) }
                .filter { (_, user) -> user != null }
                .map { (marker, user) -> marker to user!! }
                .doOnNext { (marker, user) ->
                    animateMarkerClick(marker)
                    markerClickIntent.accept(user.id to marker.position)
                }
                .switchMap { (marker, user) ->
                    val futureTarget: FutureTarget<Bitmap> = Glide.with(requireContext())
                            .asBitmap()
                            .load(user.imageUrl)
                            .submit(IMAGE_WIDTH_IN_PIXELS, IMAGE_HEIGHT_IN_PIXELS)
                    val image = Maybe.fromFuture(futureTarget)
                            .doOnDispose { futureTarget.cancel(true) }
                            .subscribeOn(io())
                    Observables.combineLatest(image.toObservable(), addressRelay)
                            .mapIndexed()
                            .observeOn(mainThread())
                            .doOnNext { (index, imageAndAddress) ->
                                infoWindowAdapter!!.setImage(imageAndAddress.first)
                                infoWindowAdapter!!.setAddress(imageAndAddress.second.value)
                                if (index == 0L) {
                                    marker.showInfoWindow()
                                } else if (marker.isInfoWindowShown) {
                                    marker.showInfoWindow()
                                }
                            }
                }
                .subscribeBy(onError = exceptionService::processException)

        cd.add(markerClick)
    }

    fun markerClickIntent(): Observable<Pair<String, LatLng>> = markerClickIntent

    private fun initViews() {
        mapObservableProvider = MapObservableProvider(mapFragment)
    }

    private fun subscribeMap() {
        viewCompositeDisposable = CompositeDisposable().apply {
            add(
                    mapAndViewReady(mapFragment, mapObservableProvider!!)
                            .doOnSuccess(::initMap)
                            .flatMapObservable {
                                usersRelay
                                        .doOnNext(::setMarkers)
                                        .switchMap { locationRelay.doOnNext(::changeLocation) }
                            }
                            .subscribe()
            )
        }
    }

    private fun unsubscribeMap() {
        viewCompositeDisposable?.let {
            it.clear()
            viewCompositeDisposable = null
        }
    }

    private fun initMap(map: GoogleMap) {
        this.map = map
        initMapUi(map)
        markersAdapter = UsersMapMarkersAdapter(map)
        infoWindowAdapter = UsersMapInfoWindowAdapter(requireContext())
    }

    private fun initMapUi(map: GoogleMap) {
        map.apply {
            clear()
            // Override the default content description on the view, for accessibility mode.
            setContentDescription(getString(R.string.mapContentDescription))
            uiSettings.apply {
                isZoomControlsEnabled = true
                isMapToolbarEnabled = true
            }
        }
    }

    private val mapFragment get() = childFragmentManager.findFragmentById(R.id.fragmentUserMap)
            as SupportMapFragment

    fun render(newViewState: UsersMapState) {
        with(newViewState) {
            users?.getContentIfNotHandled()?.let(usersRelay::accept)
            location?.getContentIfNotHandled()?.let(locationRelay::accept)
            address?.getContentIfNotHandled()?.let(addressRelay::accept)
        }
        this.viewState = newViewState
    }

    private fun setMarkers(users: List<User>) {
        val markerToUser = markersAdapter!!.setMarkers(users)
        // Setting an info window adapter allows to change the both
        // the contents and look of the info window.
        map!!.setInfoWindowAdapter(infoWindowAdapter)
        this.markerToUser = markerToUser
    }

    private fun changeLocation(location: Location) {
        markerToUser?.let { map ->
            val marker = map.entries.firstOrNull { it.value.id == location.userId }?.key
            marker?.changePositionSmoothly(location.latitude, location.longitude)
        }
    }

}