package io.bifri.usertracker.ui.main.usersmap

import android.location.Address
import io.bifri.usertracker.network.response.Location
import io.bifri.usertracker.network.response.User

sealed class PartialState

data class LoadedUsers(val users: List<User>) : PartialState()

data class LoadedLocation(val location: Location) : PartialState()

data class LoadedAddress(val address: Address?) : PartialState()