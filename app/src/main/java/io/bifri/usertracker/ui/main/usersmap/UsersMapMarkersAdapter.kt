package io.bifri.usertracker.ui.main.usersmap

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import io.bifri.usertracker.R
import io.bifri.usertracker.network.response.User

class UsersMapMarkersAdapter(private val map: GoogleMap) {

    fun setMarkers(users: List<User>): Map<Marker, User> {
        map.clear()
        val markerToUser: MutableMap<Marker, User> = hashMapOf()
        val userToMarkerOptions: MutableMap<User, MarkerOptions> = hashMapOf()
        val boundsBuilder = LatLngBounds.Builder()
        for (user in users) {
            val markerOptions = bind(user)
            userToMarkerOptions[user] = markerOptions
            boundsBuilder.include(markerOptions.position)
        }
        val bounds = boundsBuilder.build()

        // Set the camera to the greatest possible zoom level that
        // includes the bounds
        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 200))

        // Add markers
        for (user in users) {
            val marker = map.addMarker(userToMarkerOptions[user])
            markerToUser[marker] = user
        }
        return markerToUser
    }

    private fun bind(user: User) = MarkerOptions()
            .position(LatLng(user.latitude, user.longitude))
            .title(user.name)
            .icon(BitmapDescriptorFactory.fromResource(R.drawable.users_map_marker_icon))
            .snippet("")
            .flat(true)

}