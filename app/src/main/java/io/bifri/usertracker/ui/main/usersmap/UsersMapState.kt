package io.bifri.usertracker.ui.main.usersmap

import android.location.Address
import io.bifri.usertracker.network.response.Location
import io.bifri.usertracker.network.response.User
import io.bifri.usertracker.util.Event
import io.bifri.usertracker.util.Opt

data class UsersMapState(
        val users: Event<List<User>>? = null,
        val location: Event<Location>? = null,
        val address: Event<Opt<Address?>>? = null
)