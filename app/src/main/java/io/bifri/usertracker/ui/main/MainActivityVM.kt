package io.bifri.usertracker.ui.main

import android.app.Application
import io.bifri.usertracker.base.viewmodel.BaseViewModel
import io.reactivex.disposables.CompositeDisposable


class MainActivityVM(application: Application) : BaseViewModel(application) {

    override fun subscribeToDataStoreInternal(compositeDisposable: CompositeDisposable) {

    }

}