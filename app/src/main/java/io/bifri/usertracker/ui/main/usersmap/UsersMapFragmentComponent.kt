package io.bifri.usertracker.ui.main.usersmap

import dagger.BindsInstance
import dagger.Subcomponent
import io.bifri.usertracker.ui.main.usersmap.UsersMapFragment

@PerUsersMapFragment
@Subcomponent(modules = [ UsersMapFragmentModule::class ])
interface UsersMapFragmentComponent {

    @Subcomponent.Builder interface Builder {
        @BindsInstance fun usersMapFragment(fragment: UsersMapFragment): Builder

        fun build(): UsersMapFragmentComponent
    }

    fun inject(usersMapFragment: UsersMapFragment)

}
