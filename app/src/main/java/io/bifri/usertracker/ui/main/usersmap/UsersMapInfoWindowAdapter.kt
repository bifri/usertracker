package io.bifri.usertracker.ui.main.usersmap

import android.content.Context
import android.graphics.Bitmap
import android.location.Address
import android.view.LayoutInflater
import android.view.View
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter
import com.google.android.gms.maps.model.Marker
import io.bifri.usertracker.databinding.ItemUsersMapInfoWindowBinding


@Suppress("JoinDeclarationAndAssignment")
class UsersMapInfoWindowAdapter(context: Context) : InfoWindowAdapter {

    private var image: Bitmap? = null
    private var address: Address? = null
    private val itemBinding: ItemUsersMapInfoWindowBinding
    private val infoWindowVH: UsersMapInfoWindowViewHolder

    init {
        itemBinding = ItemUsersMapInfoWindowBinding.inflate(LayoutInflater.from(context))
        infoWindowVH = UsersMapInfoWindowViewHolder(itemBinding)
    }

    fun setImage(image: Bitmap) {
        this.image = image
    }

    fun setAddress(address: Address?) {
        this.address = address
    }

    override fun getInfoContents(marker: Marker): View? {
        return null
    }

    override fun getInfoWindow(marker: Marker): View {
        infoWindowVH.bind(marker, image, address)
        return itemBinding.root
    }

}