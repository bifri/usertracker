package io.bifri.usertracker.ui.main

import io.bifri.usertracker.base.viewmodel.BaseRxViewBinder
import io.bifri.usertracker.exception.ExceptionService
import io.reactivex.disposables.CompositeDisposable

class MainActivityVB(
        private val view: MainActivity,
        private val viewModel: MainActivityVM,
        exceptionService: ExceptionService
) : BaseRxViewBinder(exceptionService) {

    override fun bindInternal(cd: CompositeDisposable) {

    }

}