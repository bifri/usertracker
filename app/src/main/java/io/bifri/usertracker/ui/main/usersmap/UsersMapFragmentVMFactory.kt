package io.bifri.usertracker.ui.main.usersmap

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.bifri.usertracker.exception.ExceptionService
import io.bifri.usertracker.network.service.UserLocationService
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider

class UsersMapFragmentVMFactory(
        private val app: Application,
        private val exceptionService: ExceptionService,
        private val userLocationService: UserLocationService,
        private val locationProvider: ReactiveLocationProvider
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return UsersMapFragmentVM(
                app,
                exceptionService,
                userLocationService,
                locationProvider
        ) as T
    }

}