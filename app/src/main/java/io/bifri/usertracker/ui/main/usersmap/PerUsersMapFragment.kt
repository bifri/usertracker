package io.bifri.usertracker.ui.main.usersmap


import javax.inject.Scope

/**
 * Custom scope for UsersMapFragment singletons
 */
@Scope annotation class PerUsersMapFragment