package io.bifri.usertracker.ui.main

import dagger.BindsInstance
import dagger.Subcomponent
import io.bifri.usertracker.ui.main.usersmap.UsersMapFragmentComponent

@PerMainActivity
@Subcomponent(modules = [ MainActivityModule::class ])
interface MainActivityComponent {

    @Subcomponent.Builder interface Builder {
        @BindsInstance fun mainActivity(activity: MainActivity): Builder

        fun build(): MainActivityComponent
    }

    fun newUsersMapFragmentComponentBuilder(): UsersMapFragmentComponent.Builder

    fun inject(mainActivity: MainActivity)

}