package io.bifri.usertracker.ui.main.usersmap

import android.content.Context
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import io.bifri.usertracker.di.APP_CONTEXT
import io.bifri.usertracker.exception.ExceptionService
import io.bifri.usertracker.network.service.UserLocationService
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider
import javax.inject.Named

@Module class UsersMapFragmentModule {

    @Provides @PerUsersMapFragment fun provideUsersMapFragmentVM(
            activity: FragmentActivity,
            fragment: UsersMapFragment,
            exceptionService: ExceptionService,
            userLocationService: UserLocationService,
            locationProvider: ReactiveLocationProvider
    ): UsersMapFragmentVM = ViewModelProvider(
            fragment,
            UsersMapFragmentVMFactory(
                    activity.application,
                    exceptionService,
                    userLocationService,
                    locationProvider
            )
    )
            .get(UsersMapFragmentVM::class.java)

    @Provides @PerUsersMapFragment fun provideUsersMapFragmentVB(
            view: UsersMapFragment,
            viewModel: UsersMapFragmentVM,
            exceptionService: ExceptionService
    ): UsersMapFragmentVB = UsersMapFragmentVB(view, viewModel, exceptionService)

    @Provides @PerUsersMapFragment fun provideReactiveLocationProvider(
            @Named(APP_CONTEXT) context: Context?
    ): ReactiveLocationProvider = ReactiveLocationProvider(context)

}