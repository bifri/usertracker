package io.bifri.usertracker.ui.main.usersmap

import io.bifri.usertracker.base.viewmodel.BaseRxViewBinder
import io.bifri.usertracker.exception.ExceptionService
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy

class UsersMapFragmentVB(
        private val view: UsersMapFragment,
        private val viewModel: UsersMapFragmentVM,
        exceptionService: ExceptionService
) : BaseRxViewBinder(exceptionService) {

    override fun bindInternal(cd: CompositeDisposable) {
        cd.addAll(
                view.lifecycle
                        .subscribeBy(
                                onNext = viewModel.onLifecycleEvent,
                                onError = exceptionService::processException
                        ),
                view.markerClickIntent()
                        .subscribeBy(
                                onNext = viewModel.onMarkerClick(),
                                onError = exceptionService::processException
                        ),
                viewModel.usersMapState()
                        .subscribeBy(
                                onNext = view::render,
                                onError = exceptionService::processException
                        )
        )
    }

}