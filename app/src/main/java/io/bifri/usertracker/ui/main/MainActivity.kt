package io.bifri.usertracker.ui.main

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import io.bifri.usertracker.R
import io.bifri.usertracker.base.activity.BaseActivity
import io.bifri.usertracker.databinding.ActivityMainBinding
import io.bifri.usertracker.exception.ExceptionService
import io.bifri.usertracker.util.setupWithNavController
import javax.inject.Inject


/**
 * https://github.com/googlesamples/android-architecture-components/blob/master/NavigationAdvancedSample/app/src/main/java/com/example/android/navigationadvancedsample/MainActivity.kt
 */
class MainActivity : BaseActivity() {

    @Inject override lateinit var viewBinder: MainActivityVB
    @Inject lateinit var exceptionService: ExceptionService
    private lateinit var viewBinding: ActivityMainBinding
    private val navGraphIds = listOf(R.navigation.main)
    private val topLevelDestinationIds = setOf(R.id.usersMapFragment)
    private lateinit var appBarConfiguration: AppBarConfiguration
    private var currentNavController: LiveData<NavController>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        setSupportActionBar(viewBinding.toolbarMain)
        appBarConfiguration = AppBarConfiguration(topLevelDestinationIds)
        if (savedInstanceState == null) {
            setupNavigationViews()
        } // Else, need to wait for onRestoreInstanceState
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        // Now that BottomNavigationBar has restored its instance state
        // and its selectedItemId, we can proceed with setting up the
        // BottomNavigationBar with Navigation
        setupNavigationViews()
    }

    override fun onSupportNavigateUp() = currentNavController?.value?.navigateUp() ?: false

    /**
     * Overriding popBackStack is necessary in this case if the app is started from the deep link.
     */
    override fun onBackPressed() {
        if (currentNavController?.value?.popBackStack() != true) {
            super.onBackPressed()
        }
    }

    /**
     * Called on first creation and when restoring state.
     */
    private fun setupNavigationViews() {
        // Setup the bottom navigation view with a list of navigation graphs
        val controller = viewBinding.bottomNavigationViewMain.setupWithNavController(
                navGraphIds = navGraphIds,
                fragmentManager = supportFragmentManager,
                containerId = R.id.frameLayoutMainNavHostContainer,
                intent = intent
        )

        // Whenever the selected controller changes, setup the action bar.
        controller.observe(this, { navController ->
            viewBinding.toolbarMain.setupWithNavController(navController, appBarConfiguration)
        })
        currentNavController = controller
    }

}