package io.bifri.usertracker.exception

import dagger.Module
import dagger.Provides
import io.bifri.usertracker.di.PerApp


@Module class ExceptionModule {

    @Provides @PerApp fun provideExceptionService(): ExceptionService = DefaultExceptionService()

    @Provides @PerApp fun provideUncaughtExceptionHandler(
            exceptionService: ExceptionService
    ): Thread.UncaughtExceptionHandler =
            UncaughtExceptionHandlerGetter(exceptionService).uncaughtExceptionHandler

}