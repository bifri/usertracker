package io.bifri.usertracker.exception

interface ExceptionService {
    fun processException(t: Throwable)
}
