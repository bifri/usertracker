package io.bifri.usertracker.exception

import io.bifri.usertracker.util.loge

class DefaultExceptionService : ExceptionService {

    override fun processException(t: Throwable) {
        loge(message = t.message, throwable = t)
    }

}