package io.bifri.usertracker.app

import android.content.Context
import androidx.fragment.app.FragmentActivity
import dagger.Module
import dagger.Provides
import io.bifri.usertracker.di.ACTIVITY_CONTEXT
import io.bifri.usertracker.di.PerController
import javax.inject.Named

@Module class ControllerModule {

    @Provides @PerController @Named(ACTIVITY_CONTEXT)
    fun provideActivityContext(activity: FragmentActivity): Context = activity

}