package io.bifri.usertracker.app

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import io.bifri.usertracker.di.Injector

class ComponentInjector(val component: Any, val injector: Injector)

interface IAppInjector {

    fun activityComponentInjector(target: FragmentActivity): ComponentInjector

    fun fragmentInjector(target: Fragment, activityComponent: Any): Injector

}