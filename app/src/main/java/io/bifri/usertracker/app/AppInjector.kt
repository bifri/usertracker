package io.bifri.usertracker.app

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import io.bifri.usertracker.di.ComponentReflectionInjector
import io.bifri.usertracker.di.Injector
import io.bifri.usertracker.ui.main.MainActivity
import io.bifri.usertracker.ui.main.MainActivityComponent
import io.bifri.usertracker.ui.main.usersmap.UsersMapFragment
import io.bifri.usertracker.ui.main.usersmap.UsersMapFragmentComponent

class AppInjector(val app: App) : IAppInjector {

    override fun activityComponentInjector(target: FragmentActivity): ComponentInjector {
        val controllerComponent = app.appComponent
                .newControllerComponentBuilder()
                .activity(target)
                .build()
        return when (target::class) {
            MainActivity::class -> {
                val activityComponent = controllerComponent
                        .newMainActivityComponentBuilder()
                        .mainActivity(target as MainActivity)
                        .build()
                ComponentInjector(
                        activityComponent,
                        ComponentReflectionInjector(
                                MainActivityComponent::class.java,
                                activityComponent
                        )
                )
            }
            else -> throw IllegalArgumentException("Unsupported injection target")
        }
    }

    override fun fragmentInjector(target: Fragment, activityComponent: Any): Injector =
            when (target::class) {
                UsersMapFragment::class -> ComponentReflectionInjector(
                        UsersMapFragmentComponent::class.java,
                        (activityComponent as MainActivityComponent)
                                .newUsersMapFragmentComponentBuilder()
                                .usersMapFragment(target as UsersMapFragment)
                                .build()
                )
                else -> throw IllegalArgumentException("Unsupported injection target: ${target::class}")
            }

}