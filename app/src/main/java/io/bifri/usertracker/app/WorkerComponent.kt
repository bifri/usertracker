package io.bifri.usertracker.app

import io.bifri.usertracker.di.PerWorker
import dagger.Subcomponent

@PerWorker @Subcomponent interface WorkerComponent {

    @Subcomponent.Builder interface Builder {
        fun build(): WorkerComponent
    }

//  fun inject(sendMessagesWorker: SendMessagesWorker)

}