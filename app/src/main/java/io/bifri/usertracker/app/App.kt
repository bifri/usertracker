package io.bifri.usertracker.app

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import io.bifri.usertracker.di.APP_INJECTOR
import io.bifri.usertracker.di.ComponentReflectionInjector
import io.bifri.usertracker.di.Injector
import io.bifri.usertracker.state.TcpClientConnectionHandler
import javax.inject.Inject
import javax.inject.Named

class App : LifecycleLoggingApp(), IAppInjector {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder().application(this).build()
    }
    private val appComponentInjector: Injector by lazy {
        ComponentReflectionInjector(AppComponent::class.java, appComponent)
    }
    @field:[Inject Named(APP_INJECTOR)] lateinit var appInjector: IAppInjector
    @Inject lateinit var tcpClientConnectionHandler: TcpClientConnectionHandler
    @Inject lateinit var uncaughtExceptionHandler: Thread.UncaughtExceptionHandler

    override fun onCreate() {
        super.onCreate()
        appComponentInjector.inject(this)
        Thread.setDefaultUncaughtExceptionHandler(uncaughtExceptionHandler)
        tcpClientConnectionHandler.register()
    }

    override fun activityComponentInjector(target: FragmentActivity): ComponentInjector =
            appInjector.activityComponentInjector(target)

    override fun fragmentInjector(target: Fragment, activityComponent: Any): Injector =
            appInjector.fragmentInjector(target, activityComponent)


}