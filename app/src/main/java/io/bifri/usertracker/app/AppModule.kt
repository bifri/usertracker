package io.bifri.usertracker.app

import android.app.Application
import android.content.Context
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ProcessLifecycleOwner
import dagger.Module
import dagger.Provides
import io.bifri.usertracker.di.APP_CONTEXT
import io.bifri.usertracker.di.APP_INJECTOR
import io.bifri.usertracker.di.PROCESS_LIFECYCLE_OWNER
import io.bifri.usertracker.di.PerApp
import io.bifri.usertracker.exception.ExceptionModule
import io.bifri.usertracker.network.NetworkModule
import io.bifri.usertracker.state.StateModule
import io.bifri.usertracker.util.RxActivityLifecycleCallbacks
import io.bifri.usertracker.util.RxAppLifecycleCallbacks
import javax.inject.Named

@Module(includes = [
    ExceptionModule::class,
    NetworkModule::class,
    StateModule::class
])
class AppModule {

    @Provides @PerApp fun provideApplication(app: App): Application {
        return app
    }

    @Provides @PerApp @Named(APP_INJECTOR) fun provideAppInjector(app: App): IAppInjector {
        return AppInjector(app)
    }

    @Provides @PerApp @Named(APP_CONTEXT)
    fun provideApplicationContext(application: Application): Context =
            application.applicationContext

    @Provides @Named(PROCESS_LIFECYCLE_OWNER)
    fun provideProcessLifecycleOwner(): LifecycleOwner = ProcessLifecycleOwner.get()

    @Provides @PerApp fun provideRxAppLifecycleCallbacks(
            @Named(PROCESS_LIFECYCLE_OWNER) processLifecycleOwner: LifecycleOwner
    ): RxAppLifecycleCallbacks = RxAppLifecycleCallbacks(processLifecycleOwner)

    @Provides @PerApp fun provideRxActivityLifecycleCallbacks(
            app: App
    ): RxActivityLifecycleCallbacks = RxActivityLifecycleCallbacks(app)

}