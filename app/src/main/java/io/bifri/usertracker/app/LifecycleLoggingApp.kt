package io.bifri.usertracker.app

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import io.bifri.usertracker.util.logi

abstract class LifecycleLoggingApp : Application() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        logi("attachBaseContext()")
    }

    override fun onCreate() {
        super.onCreate()
        logi("onCreate()")
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        logi("onConfigurationChanged()")
    }

}