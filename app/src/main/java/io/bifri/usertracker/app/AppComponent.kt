package io.bifri.usertracker.app

import io.bifri.usertracker.di.PerApp
import dagger.BindsInstance
import dagger.Component

@PerApp @Component(modules = [ AppModule::class ])
interface AppComponent {

    @Component.Builder interface Builder {
        @BindsInstance fun application(app: App): AppComponent.Builder

        fun build(): AppComponent
    }

    fun newControllerComponentBuilder(): ControllerComponent.Builder

    fun newServiceComponentBuilder(): ServiceComponent.Builder

    fun newWorkerComponentBuilder(): WorkerComponent.Builder

    fun inject(app: App)

}
