package io.bifri.usertracker.app

import io.bifri.usertracker.di.PerService
import dagger.Subcomponent

@PerService @Subcomponent interface ServiceComponent {

    @Subcomponent.Builder interface Builder {
        fun build(): ServiceComponent
    }

//  fun inject(service: MyService)

}