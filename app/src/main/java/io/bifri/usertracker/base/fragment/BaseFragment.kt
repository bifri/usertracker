package io.bifri.usertracker.base.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.Relay
import io.bifri.usertracker.R
import io.bifri.usertracker.util.hideKeyboard
import io.reactivex.Observable

abstract class BaseFragment<Args : Any, ViewState : Any> : InitFragment() {
    private val initArgsIntent: Relay<Args> = BehaviorRelay.create()
    open val titleResId: Int @StringRes get() = R.string.EMPTY
    protected abstract val args: Args
    protected var viewState: ViewState? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onInitArgsIntent(args)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(layoutId, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setTitle(titleResId)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        viewState = null
    }

    override fun onDestroyView() {
        super.onDestroyView()
        requireActivity().currentFocus?.hideKeyboard()
        (parentFragment as? DialogFragment)?.dialog?.currentFocus?.hideKeyboard()
    }

    protected abstract val layoutId: Int

    private fun setTitle(@StringRes titleResId: Int) =
            (requireActivity() as AppCompatActivity).supportActionBar!!.setTitle(titleResId)

    fun initArgsIntent(): Observable<Args> = initArgsIntent

    private val onInitArgsIntent = initArgsIntent::accept

}