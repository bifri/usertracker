package io.bifri.usertracker.base.activity

interface HasActivityComponent {
    val activityComponent: Any?
}