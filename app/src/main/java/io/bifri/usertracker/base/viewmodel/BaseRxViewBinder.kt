package io.bifri.usertracker.base.viewmodel

import io.bifri.usertracker.exception.ExceptionService

abstract class BaseRxViewBinder(val exceptionService: ExceptionService) : RxViewBinder()