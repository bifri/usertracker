package io.bifri.usertracker.base.fragment

abstract class NoArgsBaseFragment<ViewState : Any> : BaseFragment<Unit, ViewState>() {

    override val args = Unit

}